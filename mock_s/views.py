from django.http import HttpResponse
from django.shortcuts import render

import json

class UgtVars:
	fact_counter = 3000
	challenge_counter = 2000 
	tour_counter = 1000 

class ErikDb:
	tours = {}
	challenges = {}
	facts = {}

def tour(request, tour_id):
	if request.method == "GET":
		tour_id_int = int(tour_id)
		print tour_id_int
		print ErikDb.tours
		tour = ErikDb.tours[tour_id_int]
		print tour
		responseDict = {}
		responseDict['tours'] = [tour,]
		return get_mock_response(json.dumps(responseDict))
	elif request.method == "PUT":
		tour_id_int = int(tour_id)
		updatedTour = json.loads(request.body)['tour']
		ErikDb.tours[tour_id_int] = updatedTour
		responseDict = {}
		responseDict['tours'] = [updatedTour]
		responseDict['tours'][0]['id'] = tour_id_int
		del responseDict['tours'][0]['description']
		return get_mock_response(json.dumps(responseDict))

# ===========================================================
# TODO: Should probably make these generic
# ===========================================================

def facts(request):
	fact_d = json.loads(request.body)['fact']
	fact_d['id'] = UgtVars.fact_counter # Stamp fact with id
	UgtVars.fact_counter += 1
	ErikDb.facts[fact_d['id']] = fact_d
	responseDict = {}
	responseDict['facts'] = [fact_d] # Expected format: {facts:[{#fact1},{#fact2}...]}
	return get_mock_response(json.dumps(responseDict))

def challenges(request):
	challenge_d = json.loads(request.body)['challenge']
	challenge_d['id'] = UgtVars.challenge_counter
	UgtVars.challenge_counter += 1
	ErikDb.challenges[challenge_d['id']] = challenge_d
	responseDict = {}
	responseDict['challenges'] = [challenge_d]
	return get_mock_response(json.dumps(responseDict))

def tours(request):
	if request.method == "POST":
		tour_d = json.loads(request.body)['tour']
		tour_d['id'] = UgtVars.tour_counter
		UgtVars.tour_counter += 1
		ErikDb.tours[tour_d['id']] = tour_d
		responseDict = {}
		responseDict['tours'] = [tour_d]
		return get_mock_response(json.dumps(responseDict))
	elif request.method == "GET":
		return get_mock_response(json.dumps(mock_tours))

# ===========================================================


def dads(request):
	import pdb; pdb.set_trace()
	dad_d = json.loads(request.body)['dad']
	dad_d['id'] = 10

	# Unpack kids
	kids_l = dad_d['kids']
	kids_l[0]['id'] = 20
	kids_l[1]['id'] = 21

	# Setup relationships
	dad_d['kids'] = [20, 21]
	kids_l[0]['dad'] = 10;
	kids_l[1]['dad'] = 10;

	# Setup jsonapi response
	responseDict = {}
	responseDict['dads'] = [dad_d,]
	responseDict['kids'] = kids_l

	return get_mock_response(json.dumps(responseDict))

def things(request):
	import pdb; pdb.set_trace()
	submittedThing = json.loads(request.body)['thing']
	submittedThing['id'] = 1000
	thingsList = [submittedThing,]
	responseDict = {'things': thingsList}
	return get_mock_response(json.dumps(responseDict))

def challenge(request, challenge_id):
	challenge = json.dumps({'challenge': mock_challenges['challenges'][int(challenge_id)-1]})
	return get_mock_response(challenge)

def get_mock_response(content_in):
	return HttpResponse(content=content_in, content_type='application/json')

def ugt_index(request):
	return render(request, 'mock_s/index.html')

mock_tours = {
	'tours': [
		{
			'id': 1,
			'title': "Greenwich Village Cupcake Tour",
			'description': "my Cupcakes"	,
			'highlights': "pink cupcakes",
			'tagline': "a cupcake for everyone",
			'startingLocation': "my house",
			'endingLocation': "outside",
			'recommendedHours': "afternoon",
			'challenges': [1,2]
		},
		{
			'id': 2,
			'title': "Pub crawl", 
			'description': "my fav pubs",
			'highlights': "lots of beer",
			'tagline': "I like beer",
			'startingLocation': "the corner pub",
			'endingLocation': "the bar",
			'recommendedHours': "night",
			'challenges': [3,4]
		},
	]
}

mock_challenges ={
		'challenges': [
		{
			'id': 1,
			'name': "Magnolia Bakery",
			'typeId': 1,
			'question': "Where are the things?",
			'hint': "Move down the street. 1",
			'answers': "Down the street.",
			'point_value': 10,
			'latitude': 40.730348,
			'longitude': -74.000219,
			'facts': [1,2],
		},
		{
			'id': 2,
			'name': "Cool bar",
			'typeId': 1,
			'question': "What beer do they serve?",
			'hint': "Move down the street. 2",
			'answers': "guiness, bud",
			'point_value': 50,
			'latitude': 40.728804,
			'longitude': -74.000476,
			'facts': [3,4],
		},
		{
			'id': 3,
			'name': "Four faced liar",
			'type': 1,
			'question': "Color?",
			'hint': "Move down the street. 3",
			'answers': "green",
			'point_value': 15,
			'latitude': 40.729291,
			'longitude': -73.997923,
			'facts': [5,6,7],
		},
		{
			'id': 4,
			'name': "Village vanguard",
			'type': 2,
			'question': "Who played here?",
			'hint': "Move down the street. 4",
			'answers': "stanley clark",
			'point_value': 40,
			'latitude': 40.727665,
			'longitude': -73.998245,
			'facts': [8,9],	
		}
	]
}

mock_facts = {
	'facts': [
	{
		'id': 1,
		'factText': "Cupcakes are delicious.",
	},
	{
		'id': 2,
		'factText': "Cakes are good too.",
	},
	{
		'id': 3,
		'factText': "Beer is good.",
	},
	{
		'id': 4,
		'factText': "mmmmm",
	},
	{
		'id': 5,
		'factText': "incredible fact one",
	},
	{
		'id': 6,
		'factText': "two wow",
	},
	{
		'id': 7,
		'factText': "much three",
	},
	{
		'id': 8,
		'factText': "Jazz!",
	},
	{
		'id': 9,
		'factText': "Trumpets!",
	},	
	]
}
