from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^tours/(\d+)$', 'mock_s.views.tour'),
    url(r'^dads?$', 'mock_s.views.dads'),
    url(r'^tours?$', 'mock_s.views.tours'),
    url(r'^facts?$', 'mock_s.views.facts'),
    url(r'^challenges?$', 'mock_s.views.challenges'),
    url(r'^challenges/(\d+)$', 'mock_s.views.challenge'),
    url(r'^facts/(\d+)$', 'mock_s.views.fact'),
    url(r'^ugt$', 'mock_s.views.ugt_index'),
    url(r'^things?$', 'mock_s.views.things'),
)
