<html>
<body>
  {% load staticfiles %}
  {% verbatim %}
  <script type="text/x-handlebars">
    <h1>Application</h1>
    {{outlet}}
  </script>
  <script type="text/x-handlebars" data-template-name="tours">
    <h1>Tours</h1>
    <button {{action 'getTour'}}>Get tour!</button>
  </script>
  <script type="text/x-handlebars" data-template-name="tour">
    <h1>Tour: {{title}}</h1>
    <button {{action 'saveTour'}}>Save tour!</button>
    <br/>
    <br/>
    {{#each challenge in controllers.challenges}}
      <div {{bind-attr class="challenge.isActive:active:hidden"}}>{{challenge.name}}</div><br/>
      <hr>

      {{fact-panel tour=controller facts=challenge.facts challenge=challenge}}

      <hr>
    {{/each}}
  </script>

  <script type="text/x-handlebars" id="components/fact-panel">
    {{#each fact in facts}}
      {{fact.factText}} isActive:{{#if fact.isActive}}yes{{else}}false{{/if}}
      <br/>
      {{#if fact.isActive}}
        {{input type=text value=fact.factText}}
      {{/if}}
      <div {{bind-attr class="fact.isActive:active:hidden"}}>
        {{view App.FileUpload tour_id=tour.id challenge_id=challenge.id fact_id=fact.id }}
      </div></br>
    {{/each}}
  </script> 
  {% endverbatim %}

  <script src={% static "js/jquery-1.10.2.min.js" %}></script>
  <script src={% static "js/handlebars-1.0.0.js" %}></script>
  <script src={% static "js/ember.js" %}></script>
  <script src={% static "js/ember-data.js" %}></script>
  <script src={% static "js/application.js" %}></script>
</body>
</html>