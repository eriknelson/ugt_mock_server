App = Ember.Application.create({
  LOG_TRANSITIONS: true,
});

App.Router.map(function() {
  this.resource('tours');
  this.resource('tour', { path: '/tours/:tour_id'});
});

/***************************************************************************/
// ROUTES
/***************************************************************************/

App.ToursRoute = Em.Route.extend({
  model: function(){
    return this.store.find('tour');
  },
});

App.TourRoute = Em.Route.extend({
  model: function(params){
    return this.store.find('tour', params.tour_id);
  },
  setupController: function(controller, model){
    console.log(model);
    controller.set('model', model);
  }
});

App.ToursController = Em.ArrayController.extend({
  actions: {
    createNewTour: function(){

      var fact = this.store.createRecord("fact", {
        factText: "factText",
        factImage: "factImage.png",
      });
      var fact2 = this.store.createRecord("fact", {
        factText: "factText 2",
        factImage: "factImage.png 2",
      });
      var fact3 = this.store.createRecord("fact", {
        factText: "factText 3",
        factImage: "factImage.png 3",
      });

      var challenge = this.store.createRecord("challenge", {
        name: "This is a new challenge",
        question: "question",
        hint: "hint",
        answers: "answers,two",
        pointValue: 10,
        latitude: 1.00,
        longitude: 1.00,
      });

      var tour = this.store.createRecord("tour", {
        title: "the newest tour",
        description: "description",
        highlights: "highlights",
        tagline: "tagline",
        startinglocation: "startinglocation",
        endinglocation: "endinglocation",
        recommendedhours: "recommendedhours"
      });

      challenge.get("facts").pushObjects([fact, fact2, fact3]);
      tour.get("challenges").pushObjects([challenge]);

      myController = this;
      challenge.get("facts").save().then(function(arg){
        challenge.save().then(function(arg){
          tour.save().then(function(arg){
            myController.transitionToRoute('tour', tour);
          });
        });
      });

    }
  }
});

App.TourController = Em.ObjectController.extend({
  actions: {
    save: function(){
      var tour = this.get('model');
      SB.Algo.getDirtyNodes(tour).forEach(function(node){
        node.save();
      });
    },
    checkDirty: function(){
      console.log("Dirty Nodes: ", SB.Algo.getDirtyNodes(tour));
    },
    logChallenge: function(challenge){
      console.log(challenge);
    }
  }    
});

DS.JSONSerializer.reopen({
    serializeHasMany: function(record, json, relationship) {
        var key = relationship.key,
            hasMany = Ember.get(record, key),
            relationshipType = DS.RelationshipChange.determineRelationshipType(record.constructor, relationship);

        if (hasMany && relationshipType === 'manyToNone' || relationshipType === 'manyToMany' ||
            relationshipType === 'manyToOne') {
            
            json[key] = [];
            console.log("HASMANY: ", hasMany);
            hasMany.forEach(function(item, index){
                console.log("ITEM: ", item);
                console.log("ITEM ID: ", item.id);
                json[key].push(parseInt(item.get('id')));
            });
            console.log(json);
        }
        else {
            return this._super(record, json, relationship);
        }
    }
});



App.Store = DS.Store.extend({
  adapter: DS.RESTAdapter.extend({
    host: 'http://localhost:8000'
  }),
});

App.Tour = DS.Model.extend({
  title: DS.attr('string'),
  description: DS.attr('string'),
  highlights: DS.attr('string'),
  tagline: DS.attr('string'),
  startingLocation: DS.attr('string'),
  endingLocation: DS.attr('string'),
  recommendedHours: DS.attr('string'),
  challenges: DS.hasMany('challenge')
});


App.Challenge = DS.Model.extend({
  name: DS.attr('string'),
  question: DS.attr('string'),
  hint: DS.attr('string'),
  answers: DS.attr('string'),
  pointValue: DS.attr('number'),
  latitude: DS.attr('number'),
  longitude: DS.attr('number'),
  tour: DS.belongsTo('tour'),
  isActive: DS.attr('boolean'),
  facts: DS.hasMany('fact') 
});

App.Fact = DS.Model.extend({
  factText: DS.attr('string'),
  factImage: DS.attr('string'),
  challenge: DS.belongsTo('challenge'),
  isActive: DS.attr('boolean')
});

// ================= OLD TESTING MODELS ======================
App.Thing = DS.Model.extend({
  name: DS.attr('string')
});

App.Dad = DS.Model.extend({
  kids: DS.hasMany('kid'),
  name: DS.attr('string')
});

App.Kid = DS.Model.extend({
  dad: DS.belongsTo('dad'),
  name: DS.attr('string')
});

// ================= TREE TRAVERSAL IMPLEMENTATION ============
SB = {Algo: {}}

SB.Algo.TreeNode = {
  create: function(hash){
    return {
      item: hash.item,
      parent: hash.parent || null,
      children: hash.children || [],
    };
  },
};

SB.Algo.visitDepthFirst = function (node, func){
  if(func){ func(node); }
  node.children.forEach(function(child){
    SB.Algo.visitDepthFirst(child, func);
  });
}

SB.Algo.getDirtyNodes = function(tour){
  dirtyNodes = []
  // SB.Algo.visitDepthFirst(root, function(node){
  //   console.log(node);
  // });
  if(tour.get('isDirty')) { dirtyNodes.push(tour); }
  tour.get('challenges').forEach(function(challenge){
    if(challenge.get('isDirty')){ dirtyNodes.push(challenge); }
    challenge.get('facts').forEach(function(fact){
      if(fact.get('isDirty')){ dirtyNodes.push(fact); }
    });
  });

  return dirtyNodes;
}

var t1 = SB.Algo.TreeNode.create({item: "t1"});

var c1 = SB.Algo.TreeNode.create({item: "c1"});
var c2 = SB.Algo.TreeNode.create({item: "c2"});

var f1 = SB.Algo.TreeNode.create({item: "f1"});
var f2 = SB.Algo.TreeNode.create({item: "f2"});
var f3 = SB.Algo.TreeNode.create({item: "f3"});
var f4 = SB.Algo.TreeNode.create({item: "f4"});

c1.parent = t1;
c1.children = [f1, f2];
c2.parent = t1;
c2.children = [f3, f4];
f1.parent = c1;
f2.parent = c1;
f3.parent = c2;
f4.parent = c2;
t1.children = [c1, c2];

// ============================================================
